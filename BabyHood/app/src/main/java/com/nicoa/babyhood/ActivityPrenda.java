package com.nicoa.babyhood;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ActivityPrenda extends AppCompatActivity {
    //nombre_prenda,descripcion,path_foto,talle

    public FloatingActionButton btn_confirmar;
    public FloatingActionButton btn_camara;
    public EditText txt_nombre_prenda;
    public EditText txt_descripcion;
    public Spinner spn_talle;
    public ImageView img_prenda;

    public String path_foto;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    public Prenda nueva_prenda;



    private ValueEventListener eventListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_prenda);

        btn_confirmar = (FloatingActionButton) findViewById(R.id.btn_confirmar);
        btn_camara = (FloatingActionButton)findViewById(R.id.btn_camara);
        txt_nombre_prenda = (EditText) findViewById(R.id.txt_nombre_prenda);
        txt_descripcion = (EditText) findViewById(R.id.txt_descripcion);
        spn_talle = (Spinner) findViewById(R.id.spn_talle);
        img_prenda = (ImageView)findViewById(R.id.img_prenda);
        path_foto = "";

        Bundle bundle_prenda =this.getIntent().getExtras();

        final DatabaseReference dbPrendas = FirebaseDatabase.getInstance().getReference().child("prendas");

        if (bundle_prenda.getString("tipo").equals("edit")) {
            String nombre_prenda = bundle_prenda.getString("nombre_prenda");
            String descripcion = bundle_prenda.getString("descripcion");
            String talle = bundle_prenda.getString("talle");



            txt_nombre_prenda.setText(nombre_prenda);
            txt_descripcion.setText(descripcion);



            int index = 0;
            switch(talle){ //TODO: Esto es horrible
                case "0-3":
                    index =0;
                    break;

                case "3-6":
                    index =1;
                    break;
                case "6-9":
                    index =2;
                    break;

                case "9-12":
                    index =3;
                    break;

                case "12-18":
                    index =4;
                    break;

                case "2":
                    index =5;
                    break;

                case "3":
                    index =6;
                    break;

                case "4":
                    index =7;
                    break;

                case "Talle Unico":
                    index =8;
                    break;

                default:
                    throw new IllegalArgumentException("Error en el Talle");
            }
            spn_talle.setSelection(index);
        }

        btn_confirmar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (!(txt_nombre_prenda.getText().toString().equals(""))) {



                        nueva_prenda = new Prenda(txt_nombre_prenda.getText().toString(),txt_descripcion.getText().toString(),null,spn_talle.getSelectedItem().toString(),0);
                        dbPrendas.child(nueva_prenda.getNombre_prenda() + "_" + nueva_prenda.getTalle()).setValue(nueva_prenda);
                        finish();
                     /*} else {
                        Context context = getApplicationContext();
                        CharSequence text = "Esa prenda con ese talle ya existe";
                        int duration = Toast.LENGTH_SHORT;

                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    }*/

                } else {
                    Context context = getApplicationContext();
                    CharSequence text = "El nombre de la prenda no puede estar en blanco";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }


            }
        });


        btn_camara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            }
        });

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            img_prenda.setImageBitmap(imageBitmap);

            path_foto = "";
            //TODO: Ver como guardar la foto
        }
    }
}