package com.nicoa.babyhood;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Maggie on 10/8/17.
 */
public class FragmentMovimientosList extends Fragment {


    public Movimiento[] movimientos;

    public ListView lst_view_movimientos;
    public FloatingActionButton btn_agregar_movimiento;


    public static FragmentMovimientosList newInstance() {
        FragmentMovimientosList fragment = new FragmentMovimientosList();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movimientos_list, container, false);

        return view;
    }

    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);
        lst_view_movimientos = (ListView)getView().findViewById(R.id.lst_view_movimientos);
        btn_agregar_movimiento = (FloatingActionButton) getView().findViewById(R.id.btn_agregar_movimiento);
        registerForContextMenu(lst_view_movimientos);
        btn_agregar_movimiento.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               // Intent intent = new Intent(getView().getContext(), ActivityAgregarMovimiento.class);

                //startActivity(intent);

            }
        });

        lst_view_movimientos.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;

        menu.setHeaderTitle("Configuracion");
        inflater.inflate(R.menu.menu_movimientos, menu);
    }






    class AdaptadorPrendaMovimientos extends ArrayAdapter<Movimiento> {

        public AdaptadorPrendaMovimientos(Context context, Movimiento[] movimientos) {
            super(context, R.layout.item_list_movimiento, movimientos);
        }

        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = LayoutInflater.from(getContext());

            View item = inflater.inflate(R.layout.item_list_movimiento, null);

            TextView lbl_tipo_movimiento = (TextView)item.findViewById(R.id.lbl_tipo_movimiento);
            TextView lbl_cantidad = (TextView) item.findViewById(R.id.lbl_cantidad);
            TextView lbl_nombre_prenda = (TextView) item.findViewById(R.id.lbl_nombre_prenda);
            TextView lbl_talle = (TextView) item.findViewById(R.id.lbl_talle);
            TextView nombre_usuario = (TextView) item.findViewById(R.id.nombre_usuario);

            lbl_tipo_movimiento.setText(movimientos[position].getTipo_movimiento());
            lbl_cantidad.setText(String.valueOf(movimientos[position].getCantidad()));
            lbl_nombre_prenda.setText(movimientos[position].getPrenda().getNombre_prenda());
            lbl_talle.setText(movimientos[position].getPrenda().getTalle());
            nombre_usuario.setText(movimientos[position].getNombre_usuario());
            return (item);
        }
    }
}

