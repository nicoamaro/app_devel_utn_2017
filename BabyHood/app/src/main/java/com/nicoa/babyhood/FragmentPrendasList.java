package com.nicoa.babyhood;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Maggie on 10/8/17.
 */
public class FragmentPrendasList extends Fragment {

    public FloatingActionButton btn_agregar_prenda;
    public FirebaseRecyclerAdapter mAdapter;
    public  DatabaseReference dbPrendas;

    public static FragmentPrendasList newInstance() {
        FragmentPrendasList fragment = new FragmentPrendasList();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_prendas_list, container, false);
        return view;
    }

    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);

        btn_agregar_prenda = (FloatingActionButton) getView().findViewById(R.id.btn_agregar_prenda);

        btn_agregar_prenda.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getView().getContext(), ActivityPrenda.class);
                Bundle bundle_prenda = new Bundle();
                bundle_prenda.putString( "tipo" ,"new");
                intent.putExtras(bundle_prenda);
                startActivity(intent);
            }
        });


        dbPrendas = FirebaseDatabase.getInstance().getReference().child("prendas");
        RecyclerView recycler = (RecyclerView) getView().findViewById(R.id.lst_view_prendas);
        recycler.setHasFixedSize(true);

        recycler.setLayoutManager(new LinearLayoutManager(getView().getContext()));

        mAdapter =
                new PrendaAdapter(Prenda.class, R.layout.item_list_prenda, PrendaHolder.class, dbPrendas, getView().getContext());

        recycler.setAdapter(mAdapter);
        registerForContextMenu(recycler);
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int position = -1;
        try {
            position = PrendaAdapter.getPosition();
        } catch (Exception e) {
//            Log.d(TAG, e.getLocalizedMessage(), e);
            return super.onContextItemSelected(item);
        }
        String nombre_prenda =((Prenda) mAdapter.getItem(position)).getNombre_prenda();
        String talle = ((Prenda) mAdapter.getItem(position)).getTalle();
        String key = nombre_prenda + "_" + talle;
        String descripcion = ((Prenda) mAdapter.getItem(position)).getDescripcion();
        switch (item.getItemId()) {
            case 1:
                // Edit Prenda
                Intent intent = new Intent(getView().getContext(), ActivityPrenda.class);
                Bundle bundle_prenda = new Bundle();
                bundle_prenda.putString("descripcion",descripcion );
                bundle_prenda.putString( "talle" ,talle);
                bundle_prenda.putString( "nombre_prenda" ,nombre_prenda);
                bundle_prenda.putString( "tipo" ,"edit");
                intent.putExtras(bundle_prenda);
                startActivity(intent);

                break;
            case 2:
                // Delete Prenda
                if (position!= -1) {
                    dbPrendas.child(key).setValue(null);
                    Toast toast1 = Toast.makeText(getActivity(), "Prenda Eliminada", Toast.LENGTH_SHORT);
                    toast1.setDuration(Toast.LENGTH_SHORT);
                    toast1.show();
                }
                break;

        }
        return super.onContextItemSelected(item);
    }




}

