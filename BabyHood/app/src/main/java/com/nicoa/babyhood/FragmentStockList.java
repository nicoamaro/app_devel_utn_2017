package com.nicoa.babyhood;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.ui.ProgressDialogHolder;

/**
 * Created by Maggie on 10/8/17.
 */
public class FragmentStockList extends Fragment {

    public Prenda[] prendas;
    public ListView lst_view_stock;
    public FloatingActionButton btn_agregar_movimiento;
    public ProgressBar pbarProgreso;
    public Context mainHilos;
    public ProgressDialog pDialog;
    public AsyncTasking tarea2;


    public static FragmentStockList newInstance() {
        FragmentStockList fragment = new FragmentStockList();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stock_list, container, false);
        return view;
    }

    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);
        lst_view_stock = (ListView)getView().findViewById(R.id.lst_view_stock);
        pbarProgreso = (ProgressBar)getView().findViewById(R.id.pbarProgreso) ;
        mainHilos = getView().getContext();
        btn_agregar_movimiento = (FloatingActionButton) getView().findViewById(R.id.btn_agregar_movimiento);

        btn_agregar_movimiento.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               // Intent intent = new Intent(getView().getContext(), ActivityAgregarMovimiento.class);

               // startActivity(intent);
                pDialog = new ProgressDialog(mainHilos);
                pDialog.setMessage("Procesando...");
                pDialog.setCancelable(true);
                pDialog.setMax(100);

                tarea2 = new AsyncTasking();
                tarea2.execute();

            }
        });


    }

    public class AsyncTasking extends AsyncTask<Void, Integer, Boolean> {

        private void tareaLarga()
        {
            try {
                Thread.sleep(1000);
            } catch(InterruptedException e) {}
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            for(int i=1; i<=10; i++) {
                tareaLarga();

                publishProgress(i*10);

                if(isCancelled())
                    break;
            }

            return true;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            int progreso = values[0].intValue();

            pbarProgreso.setProgress(progreso);
        }

        @Override
        protected void onPreExecute() {
            pbarProgreso.setMax(100);
            pbarProgreso.setProgress(0);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(result)
                Toast.makeText(mainHilos, "Tarea finalizada!",
                        Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onCancelled() {
            Toast.makeText(mainHilos, "Tarea cancelada!",
                    Toast.LENGTH_SHORT).show();
        }
    }



}

