package com.nicoa.babyhood;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Maggie on 10/8/17.
 */
public class FragmentUserList extends Fragment {

    public FirebaseRecyclerAdapter mAdapter;


    public static FragmentUserList newInstance() {
        FragmentUserList fragment = new FragmentUserList();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_list, container, false);
        return view;
    }

    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);




        DatabaseReference dbusuarios = FirebaseDatabase.getInstance().getReference().child("usuarios");
        RecyclerView recycler = (RecyclerView) getView().findViewById(R.id.lstUsuarios);
        recycler.setHasFixedSize(true);

        recycler.setLayoutManager(new LinearLayoutManager(getView().getContext()));

        mAdapter =
                new FirebaseRecyclerAdapter<Usuario, UsuarioHolder>(
                        Usuario.class, R.layout.item_list_usuario, UsuarioHolder.class, dbusuarios) {

                    @Override
                    public void populateViewHolder(UsuarioHolder predViewHolder, Usuario user, int position) {
                        predViewHolder.setUsuario(user.getNombre());
                        predViewHolder.setPermiso(user.getPermiso());
                    }
                };

        recycler.setAdapter(mAdapter);







        /*recycler.setOnClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

               //Intent intent = new Intent(getView().getContext(), ActivityEditarUsuario.class);
                //Bundle cosa = new Bundle();

                //cosa.putString("Usuario",((Usuario)a.getItemAtPosition(position)).getUsuario().toString());
                //cosa.putString("Password",((Usuario)a.getItemAtPosition(position)).getPassword().toString());
                //cosa.putString("Permiso", ((Usuario)a.getItemAtPosition(position)).getPermiso().toString());

                //intent.putExtras(cosa);
                //startActivity(intent);

           }
        });*/


    }


   // @Override
   /* public void onStart(){
        super.onStart();
        BabyHoodStockSQLiteHelper dbUsrHelper = new BabyHoodStockSQLiteHelper(getView().getContext(), "dbBabyHoodStock", null, 1);
        SQLiteDatabase dbUsr = dbUsrHelper.getReadableDatabase();
        Cursor c = dbUsr.rawQuery(" SELECT nombre,password,permisos FROM TblUsuarios ", null);

        usuarios = new Usuario[c.getCount()];
        int i=0;
        //Nos aseguramos de que existe al menos un registro
        if (c.moveToFirst()) {
            //Recorremos el cursor hasta que no haya más registros
            do {
                String usuario = c.getString(0);
                String password = c.getString(1);
                String permiso  = c.getString(2);

                usuarios[i] = new Usuario(usuario,password,permiso);
                i++;

            } while(c.moveToNext());
        }

        AdaptadorUsuario adaptador  = new AdaptadorUsuario(getView().getContext(), usuarios);
        lst_view.setAdapter(adaptador);

    } */


    /*class AdaptadorUsuario extends ArrayAdapter<Usuario> {

        public AdaptadorUsuario(Context context, Usuario[] usuarios) {
            super(context, R.layout.item_list_usuario, usuarios);
        }

        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = LayoutInflater.from(getContext());

            View item = inflater.inflate(R.layout.item_list_usuario, null);

            TextView lbl_nombre = (TextView) item.findViewById(R.id.lbl_usuario);
            TextView lbl_password = (TextView) item.findViewById(R.id.lbl_password);
            TextView lbl_permiso = (TextView) item.findViewById(R.id.lbl_permiso);

            lbl_nombre.setText(usuarios[position].getUsuario());
            lbl_password.setText(usuarios[position].getPassword());
            lbl_permiso.setText(usuarios[position].getPermiso());


            return (item);
        }
    }*/
}

