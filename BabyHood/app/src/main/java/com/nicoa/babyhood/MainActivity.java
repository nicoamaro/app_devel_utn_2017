package com.nicoa.babyhood;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public FirebaseRecyclerAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);





        DatabaseReference dbusuarios = FirebaseDatabase.getInstance().getReference().child("usuarios");
        RecyclerView recycler = (RecyclerView) findViewById(R.id.lstUsuarios);
        recycler.setHasFixedSize(true);

        recycler.setLayoutManager(new LinearLayoutManager(this));

         mAdapter =
                new FirebaseRecyclerAdapter<Usuario, UsuarioHolder>(
                        Usuario.class, R.layout.usuario_list, UsuarioHolder.class, dbusuarios) {

                    @Override
                    public void populateViewHolder(UsuarioHolder predViewHolder, Usuario user, int position) {
                        predViewHolder.setUsuario(user.getNombre());
                        predViewHolder.setPermiso(user.getPermiso());
                    }
                };

        recycler.setAdapter(mAdapter);
        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    public void onComplete(@NonNull Task<Void> task) {
                        // ...
                    }
                });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAdapter.cleanup();
        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    public void onComplete(@NonNull Task<Void> task) {
                        // ...
                    }
                });
    }
}
