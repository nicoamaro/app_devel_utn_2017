package com.nicoa.babyhood;

/**
 * Created by Maggie on 10/5/17.
 */

public class Prenda {

    private String nombre_prenda;
    private String descripcion;
    private String path_foto;
    private String talle;
    private int stock_disponible;

    public Prenda (){

    }

    public Prenda(String nombre_prenda, String descripcion, String path_foto, String talle, int stock_disponible){
        this.nombre_prenda=nombre_prenda;
        this.descripcion=descripcion;
        this.path_foto=path_foto;
        this.talle=talle;
        this.stock_disponible = stock_disponible;
    }

    public String getDescripcion() {
        return descripcion;
    }
    public String getPath_foto() {
        return path_foto;
    }
    public String getNombre_prenda() {
        return nombre_prenda;
    }
    public int getStock_disponible() {
        return stock_disponible;
    }
    public String getTalle() {
        return talle;
    }

    public void setDescripcion(String descripcion) {

        this.descripcion = descripcion;
    }

    public void setPath_foto(String foto_path) {

        this.path_foto = foto_path;
    }


    public void setNombre_prenda(String nombre_prenda) {

        this.nombre_prenda = nombre_prenda;
    }

    public void setStock_disponible(int stock_disponible) {
        this.stock_disponible = stock_disponible;
    }

    public void setTalle(String talle) {
        this.talle = talle;
    }

    public int ingresarMovimiento(String _tipo, int _canditad){
        int signo = 0;

        if(_tipo.equals("Entrada")){
            signo=1;
        }else if (_tipo.equals("Salida")){
            signo=-1;
        }else{
            return -1;
        }

        stock_disponible+=(signo*_canditad);
        return 0;
    }

    @Override
    public String toString() {
        return "Prenda{" +
                "nombre_prenda=" + nombre_prenda +
                ", descripcion=" + descripcion +
                ", path_foto=" + path_foto +
                ", talle=" + talle +
                ", stock_disponible=" + stock_disponible +
                '}';
    }

}

