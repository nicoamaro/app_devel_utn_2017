package com.nicoa.babyhood;

import android.content.Context;
import android.view.View;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;

/**
 * Created by Maggie on 23/11/2017.
 */

public class PrendaAdapter extends FirebaseRecyclerAdapter<Prenda, PrendaHolder> {
    private static int position;
    private Context context;


    public static int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public PrendaAdapter(Class<Prenda> prendaClass, int modelLayout, Class<PrendaHolder> prendaHolderClass, DatabaseReference ref, Context context) {
        super(prendaClass, modelLayout, prendaHolderClass, ref);
        this.context = context;
    }

    @Override
    protected void populateViewHolder(PrendaHolder viewHolder, Prenda prenda, final int position) {
        //do something
        viewHolder.setNombre_prenda(prenda.getNombre_prenda());
        viewHolder.setTalle(prenda.getTalle());
        viewHolder.setPath_foto(prenda.getPath_foto());

        viewHolder.mView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                setPosition(position);
                return false;
            }

        });
    }
}
