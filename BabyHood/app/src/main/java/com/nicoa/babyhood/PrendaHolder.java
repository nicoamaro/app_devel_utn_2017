package com.nicoa.babyhood;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Maggie on 16/11/2017.
 */

public class PrendaHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener{

    public View mView;



    public PrendaHolder(View itemView) {
        super(itemView);
        mView = itemView;
        itemView.setOnCreateContextMenuListener(this);
    }

    public void setNombre_prenda(String nombre_prenda) {
        TextView field = (TextView) mView.findViewById(R.id.lbl_nombre_prenda);
        field.setText(nombre_prenda);
    }


    public void setTalle(String talle) {
        TextView field = (TextView) mView.findViewById(R.id.lbl_talle);
        field.setText(talle);
    }

    public void setPath_foto(String path_foto) {
        ImageView field = (ImageView) mView.findViewById(R.id.img_prenda);
        //field.setImageBitmap(null);
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        menu.add(0,1 , 0, "Edit");
        menu.add(0, 2, 0, "Delete");
    }


}
