package com.nicoa.babyhood;

/**
 * Created by Maggie on 14/11/2017.
 */

public class Usuario {

    private String nombre;
    private String permiso;

    public Usuario(){

    }

    public Usuario(String usuario, String permiso){
        this.nombre = usuario;
        this.permiso = permiso;
    }

    public String getPermiso() {
        return permiso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setUsuario(String nombre) {
        this.nombre = nombre;
    }
    public void setPermiso(String permiso) {
        this.permiso = permiso;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "nombre='" + nombre + '\'' +
                ", permiso=" + permiso +
                '}';
    }


}
