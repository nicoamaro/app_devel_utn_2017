package com.nicoa.babyhood;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Maggie on 16/11/2017.
 */

public class UsuarioHolder extends RecyclerView.ViewHolder {

    private View mView;

    public UsuarioHolder(View itemView) {
        super(itemView);
        mView = itemView;
    }

    public void setUsuario(String usuario) {
        TextView field = (TextView) mView.findViewById(R.id.lblUsuario);
        field.setText(usuario);
    }


    public void setPermiso(String permiso) {
        TextView field = (TextView) mView.findViewById(R.id.lblPermiso);
        field.setText(permiso);
    }


}
