package com.nicoa.babyhoodstock;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityAgregarMovimiento extends AppCompatActivity {
    //nombre_prenda,descripcion,path_foto,talle

    public FloatingActionButton btn_confirmar;

    public EditText txt_nombre_prenda;
    public EditText txt_cantidad;
    public Spinner spn_usuario;
    public Spinner spn_prendas;
    public Spinner spn_tipo_movimiento;


    public SQLiteDatabase dbBaby;
    public Usuario[] usuarios;
    public Prenda[] prendas;
    public String Action = "Add";

    public Bundle cosa;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_movimiento);

        btn_confirmar = (FloatingActionButton) findViewById(R.id.btn_confirmar);

        txt_nombre_prenda = (EditText) findViewById(R.id.txt_nombre_prenda);
        txt_cantidad = (EditText) findViewById(R.id.txt_cantidad);
        spn_usuario = (Spinner) findViewById(R.id.spn_usuario);
        spn_prendas = (Spinner) findViewById(R.id.spn_prendas);
        spn_tipo_movimiento = (Spinner) findViewById(R.id.spn_tipo_movimiento);


        cargarDatos();
        //FIXME: Esto es nuevo y no esta probado

        cosa = this.getIntent().getExtras();
        Action = cosa.getString("Action");
        if (Action.equals("Edit")){
            int id_movimiento = cosa.getInt("id_movimiento");
            //id_movimiento,nombre_usuario,id_prenda,tipo_movimiento,cantidad
            Cursor c = dbBaby.rawQuery("SELECT id_movimiento,nombre_usuario,id_prenda,tipo_movimiento,cantidad FROM TblMovimientosStock WHERE id_movimiento=" + id_movimiento, null);
            if (c.moveToFirst()){
                int j = c.getCount();
                String nombre_usuario = c.getString(1);
                int id_prenda = c.getInt(2);
                String tipo_movimiento = c.getString(3);
                int Cantidad = c.getInt(4);

                int i=0;
                while(!usuarios[i].getUsuario().equals(nombre_usuario)){
                    i++;
                }
                spn_usuario.setSelection(i);
                i=0;

                while(prendas[i].getIdPrenda()!=id_prenda){
                    i++;
                }
                spn_prendas.setSelection(i);
                if (tipo_movimiento.equals("Entrada")){
                    spn_tipo_movimiento.setSelection(0);
                }else{
                    spn_tipo_movimiento.setSelection(1);
                }
                txt_cantidad.setText(Cantidad);
            }else{
                Toast toast1 = Toast.makeText(this, "Error Reading DB", Toast.LENGTH_SHORT);
                toast1.setDuration(Toast.LENGTH_SHORT);
                toast1.show();
                finish();
            }

        }



    }

    void cargarDatos(){

        BabyHoodStockSQLiteHelper dbBabyHelper = new BabyHoodStockSQLiteHelper(this, "dbBabyHoodStock", null, 1);
        dbBaby = dbBabyHelper.getReadableDatabase();
        Cursor c = dbBaby.rawQuery(" SELECT nombre FROM TblUsuarios ", null);

        String[] nombreUsuarios = new String[c.getCount()];

        int i=0;
        if (c.moveToFirst()) {
            do {
                String usuario = c.getString(0);
                nombreUsuarios[i] = usuario;
                i++;

            } while(c.moveToNext());
        }
        ArrayAdapter<String> adaptadorUsuarios = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, nombreUsuarios);

        adaptadorUsuarios.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_usuario.setAdapter(adaptadorUsuarios);


        c = dbBaby.rawQuery(" SELECT id_prenda,nombre_prenda,talle FROM TblPrendas ", null);

        String[] prendasNombreTalle = new String[c.getCount()];

        prendas = new Prenda[c.getCount()];
        i=0;
        //Nos aseguramos de que existe al menos un registro
        if (c.moveToFirst()) {
            //Recorremos el cursor hasta que no haya más registros
            do {
                int id_prenda           = c.getInt(0);
                String nombre_prenda    = c.getString(1);
                String talle            = c.getString(2);
                prendasNombreTalle[i] = nombre_prenda + ": " + talle;
                prendas[i] = new Prenda(id_prenda,nombre_prenda,null,null,talle,0);
                i++;

            } while(c.moveToNext());
        }
        ArrayAdapter<String> adaptadorPrendas = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, prendasNombreTalle);

        adaptadorPrendas.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spn_prendas.setAdapter(adaptadorPrendas);
        btn_confirmar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                btnConfirmarClick();
            }
        });
    }

    void btnConfirmarClick(){


        if(!txt_cantidad.getText().toString().equals("")){
            //id_movimiento,nombre_usuario,id_prenda,tipo_movimiento,cantidad
            Cursor c = dbBaby.rawQuery(" SELECT id_movimiento FROM TblMovimientosStock ORDER BY id_movimiento DESC", null);
            c.moveToFirst();

            int id_movimiento =c.getInt(0) + 1;
            String nombre_usuario = spn_usuario.getSelectedItem().toString();
            int id_prenda = prendas[spn_prendas.getSelectedItemPosition()].getIdPrenda();
            String tipo_movimiento = spn_tipo_movimiento.getSelectedItem().toString();
            String cantidad = txt_cantidad.getText().toString();

            dbBaby.execSQL("INSERT INTO TblMovimientosStock (id_movimiento,nombre_usuario,id_prenda,tipo_movimiento,cantidad) VALUES (" +
                    String.valueOf(id_movimiento) + "," +
                    "'" + nombre_usuario + "'," +
                    String.valueOf(id_prenda)   + "," +
                    "'"+ tipo_movimiento +"',"+
                            cantidad + ")");
            finish();



        }else{
            Context context = getApplicationContext();
            CharSequence text = "La cantidad no puede estar en blanco";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();


        }




    }



}