package com.nicoa.babyhoodstock;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ActivityAgregarPrenda extends AppCompatActivity {
    //nombre_prenda,descripcion,path_foto,talle

    public FloatingActionButton btn_confirmar;
    public FloatingActionButton btn_camara;
    public EditText txt_nombre_prenda;
    public EditText txt_descripcion;
    public Spinner spn_talle;
    public ImageView img_prenda;

    public String path_foto;
    static final int REQUEST_IMAGE_CAPTURE = 1;

    public SQLiteDatabase dbPrendas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_prenda);

        btn_confirmar = (FloatingActionButton) findViewById(R.id.btn_confirmar);
        btn_camara = (FloatingActionButton)findViewById(R.id.btn_camara);
        txt_nombre_prenda = (EditText) findViewById(R.id.txt_nombre_prenda);
        txt_descripcion = (EditText) findViewById(R.id.txt_descripcion);
        spn_talle = (Spinner) findViewById(R.id.spn_talle);
        img_prenda = (ImageView)findViewById(R.id.img_prenda);
        path_foto = "";


        BabyHoodStockSQLiteHelper dbUsrHelper = new BabyHoodStockSQLiteHelper(this, "dbBabyHoodStock", null, 1);
        dbPrendas = dbUsrHelper.getWritableDatabase();


        btn_confirmar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (!(txt_nombre_prenda.getText().toString().equals(""))) {

                    Cursor c = dbPrendas.rawQuery("SELECT nombre_prenda FROM TblPrendas WHERE " +
                            "nombre_prenda='" + txt_nombre_prenda.getText().toString() + "' AND " +
                            "talle='" + spn_talle.getSelectedItem().toString() + "'", null);

                    if (c.getCount() == 0) {

                        c = dbPrendas.rawQuery(" SELECT id_prenda FROM TblPrendas ORDER BY id_prenda DESC", null);
                        c.moveToFirst();
                        int id_prenda =c.getInt(0);

                        dbPrendas.execSQL("INSERT INTO TblPrendas (id_prenda,nombre_prenda,descripcion,path_foto,talle) VALUES (" +
                                        String.valueOf(id_prenda + 1) + ", '" +
                                        txt_nombre_prenda.getText().toString() + "'," +
                                        "'" + txt_descripcion.getText().toString()   + "'," +
                                        "'" + path_foto +  "'," +
                                        "'" + spn_talle.getSelectedItem().toString() + "')");
                        finish();
                    } else {
                        Context context = getApplicationContext();
                        CharSequence text = "Esa prenda con ese talle ya existe";
                        int duration = Toast.LENGTH_SHORT;

                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    }

                } else {
                    Context context = getApplicationContext();
                    CharSequence text = "El nombre de la prenda no puede estar en blanco";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }


            }
        });

        btn_camara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            }
        });

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            img_prenda.setImageBitmap(imageBitmap);

            path_foto = "";
            //TODO: Ver como guardar la foto
        }
    }
}