package com.nicoa.babyhoodstock;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class ActivityAgregarUsuario extends AppCompatActivity {

    public FloatingActionButton btn_confirmar;
    public EditText txt_usuario;
    public EditText txt_password;
    public Spinner spn_permiso;


    public SQLiteDatabase dbUsr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_usuario);

        btn_confirmar = (FloatingActionButton) findViewById(R.id.btn_confirmar);
        txt_usuario = (EditText)findViewById(R.id.txt_usuario);
        txt_password = (EditText)findViewById(R.id.txt_password);
        spn_permiso = (Spinner)findViewById(R.id.spn_permiso);

        BabyHoodStockSQLiteHelper dbUsrHelper = new BabyHoodStockSQLiteHelper(this, "dbBabyHoodStock", null, 1);
        dbUsr= dbUsrHelper.getWritableDatabase();

        btn_confirmar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (!(txt_usuario.getText().toString().equals(""))){

                    Cursor c = dbUsr.rawQuery(" SELECT nombre,password,permisos FROM TblUsuarios WHERE nombre='"
                            + txt_usuario.getText().toString() +"'", null);
                    if (c.getCount()==0) {
                        dbUsr.execSQL("INSERT INTO TblUsuarios (nombre,password,permisos) VALUES ('" + txt_usuario.getText().toString() + "','" + Crypto.md5(txt_password.getText().toString()) + "','" + spn_permiso.getSelectedItem().toString() + "' ) ");
                        finish();
                    }else{
                        Context context = getApplicationContext();
                        CharSequence text = "El usuario ya existe";
                        int duration = Toast.LENGTH_SHORT;

                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    }

                }else{
                    Context context = getApplicationContext();
                    CharSequence text = "El usuario no puede estar en blanco";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }


            }
        });
    }
}
