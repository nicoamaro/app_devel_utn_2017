package com.nicoa.babyhoodstock;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityEditarPrenda extends AppCompatActivity {


    public FloatingActionButton btn_borrar;
    public FloatingActionButton btn_confirmar;
    public FloatingActionButton btn_camara;
    public EditText txt_nombre_prenda;
    public EditText txt_descripcion;
    public Spinner spn_talle;
    public ImageView img_prenda;
    public Prenda prenda;

    public String path_foto;
    static final int REQUEST_IMAGE_CAPTURE = 1;

    public SQLiteDatabase dbPrendas;
    public Bundle cosa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_prenda);

        btn_confirmar = (FloatingActionButton) findViewById(R.id.btn_confirmar);
        btn_borrar = (FloatingActionButton) findViewById(R.id.btn_borrar);
        btn_camara = (FloatingActionButton) findViewById(R.id.btn_camara);

        txt_nombre_prenda = (EditText)findViewById(R.id.txt_nombre_prenda);
        txt_descripcion = (EditText)findViewById(R.id.txt_descripcion);
        spn_talle = (Spinner)findViewById(R.id.spn_talle) ;

        cosa = this.getIntent().getExtras();

        BabyHoodStockSQLiteHelper dbUsrHelper = new BabyHoodStockSQLiteHelper(this, "dbBabyHoodStock", null, 1);
        dbPrendas = dbUsrHelper.getWritableDatabase();
        //id_prenda,nombre_prenda,descripcion,path_foto,talle
        Cursor c = dbPrendas.rawQuery("SELECT id_prenda,nombre_prenda,descripcion,path_foto,talle FROM TblPrendas WHERE " +
                "id_prenda=" +String.valueOf(cosa.getInt("id_prenda")), null);

        if (c.getCount()!=1){
                throw new IllegalAccessError("Inconsistencia en la dB, id_prenda no encontrado");
        }

        c.moveToFirst();
        prenda = new Prenda(c.getInt(0),c.getString(1),c.getString(2),c.getString(3),c.getString(4),0);

        txt_nombre_prenda.setText(prenda.getNombre_prenda());
        txt_descripcion.setText(prenda.getDescripcion());

        String talle = prenda.getTalle();

        int index = 0;
        switch(talle){ //TODO: Esto es horrible
            case "0-3":
                index =0;
                break;

            case "3-6":
                index =1;
                break;
            case "6-9":
                index =2;
                break;

            case "9-12":
                index =3;
                break;

            case "12-18":
                index =4;
                break;

            case "2":
                index =5;
                break;

            case "3":
                index =6;
                break;

            case "4":
                index =7;
                break;

            case "Talle Unico":
                index =8;
                break;

            default:
                throw new IllegalArgumentException("Error en el Talle");
        }
        spn_talle.setSelection(index);


        btn_confirmar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                btnConfirmarClick();
            }
        });

        btn_borrar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               btnBorrarClick();
            }
        });

        btn_camara.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            img_prenda.setImageBitmap(imageBitmap);

            path_foto = "";
            //TODO: Ver como guardar la foto
        }
    }

    void btnConfirmarClick (){


        if (!(txt_nombre_prenda.getText().toString().equals(""))) {
            Cursor c = dbPrendas.rawQuery(" SELECT id_prenda FROM TblPrendas WHERE nombre_prenda='"+ txt_nombre_prenda.getText().toString() +
                    "' AND talle='"+spn_talle.getSelectedItem().toString()+"'", null);
            if(c.getCount()==0) {
                dbPrendas.execSQL("UPDATE TblPrendas SET " +
                        "nombre_prenda='" + txt_nombre_prenda.getText().toString() + "', " +
                        "talle='"+ spn_talle.getSelectedItem().toString() +"'"+
                        "WHERE " +
                        "id_prenda=" + String.valueOf(prenda.getIdPrenda()));
            }else if(!(prenda.getNombre_prenda().equals(txt_nombre_prenda.getText().toString()) && prenda.getTalle().equals(spn_talle.getSelectedItem().toString()))) {

                Context context = getApplicationContext();
                CharSequence text = "Ya existe esa prenda con ese talle";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
                return;
            }
        }else{
            Context context = getApplicationContext();
            CharSequence text = "El nombre de la prenda no puede estar en blanco";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
            return;
        }
        if (!(txt_descripcion.getText().toString().equals(prenda.getDescripcion()))){
            dbPrendas.execSQL("UPDATE TblPrendas SET " +
                    "descripcion='" + txt_descripcion.getText().toString() + "' " +
                    "WHERE " +
                    "id_prenda=" + String.valueOf(prenda.getIdPrenda()));
        }


        finish();

    }

    void btnBorrarClick () {

                dbPrendas.execSQL("DELETE FROM TblMovimientosStock WHERE id_prenda="+ String.valueOf(prenda.getIdPrenda()));
                dbPrendas.execSQL("DELETE FROM TblPrendas WHERE id_prenda="+ String.valueOf(prenda.getIdPrenda()));
                finish();

    }

}
