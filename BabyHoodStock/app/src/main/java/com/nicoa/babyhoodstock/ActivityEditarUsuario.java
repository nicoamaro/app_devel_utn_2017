package com.nicoa.babyhoodstock;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityEditarUsuario extends AppCompatActivity {

    public FloatingActionButton btn_confirmar;
    public FloatingActionButton btn_borrar;
    public TextView lbl_usuario;
    public EditText txt_password;
    public Spinner spn_permiso;

    public SQLiteDatabase dbUsr;
    public Bundle cosa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_usuario);

        btn_confirmar = (FloatingActionButton) findViewById(R.id.btn_confirmar);
        btn_borrar = (FloatingActionButton) findViewById(R.id.btn_borrar);
        lbl_usuario = (TextView)findViewById(R.id.lbl_usuario);
        txt_password = (EditText)findViewById(R.id.txt_password);
        spn_permiso = (Spinner)findViewById(R.id.spn_permiso);

        cosa = this.getIntent().getExtras();
        lbl_usuario.setText(cosa.getString("Usuario"));
        txt_password.setText(cosa.getString("Password"));

        if (cosa.getString("Permiso").equals("admin")){
            spn_permiso.setSelection(1);
        }else {
            spn_permiso.setSelection(0);
        }

        BabyHoodStockSQLiteHelper dbUsrHelper = new BabyHoodStockSQLiteHelper(this, "dbBabyHoodStock", null, 1);
        dbUsr= dbUsrHelper.getWritableDatabase();

        btn_confirmar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!(txt_password.getText().toString().equals(cosa.getString("Password")))) {
                    dbUsr.execSQL("UPDATE TblUsuarios SET " +
                            "password='" + Crypto.md5(txt_password.getText().toString()) + "' " +
                            "WHERE " +
                            "nombre='" + lbl_usuario.getText().toString() + "'");

                }

                if (!(spn_permiso.getSelectedItem().toString().equals(cosa.getString("Permiso")))) {
                    if (cosa.getString("Permiso").toString().equals("admin")) {
                        Cursor c = dbUsr.rawQuery(" SELECT nombre,password,permisos FROM TblUsuarios WHERE permisos='admin'", null);
                        if (c.getCount() > 1) {
                            dbUsr.execSQL("UPDATE TblUsuarios SET " +
                                    "permisos='" + spn_permiso.getSelectedItem().toString() + "' " +
                                    "WHERE " +
                                    "nombre='" + lbl_usuario.getText().toString() + "'");
                        } else {
                            Context context = getApplicationContext();
                            CharSequence text = "Es el ultimo admin, no se puede cambiar el permiso";
                            int duration = Toast.LENGTH_SHORT;

                            Toast toast = Toast.makeText(context, text, duration);
                            toast.show();
                            return;
                        }

                    }else{
                        dbUsr.execSQL("UPDATE TblUsuarios SET " +
                                "permisos='" + spn_permiso.getSelectedItem().toString() + "' " +
                                "WHERE " +
                                "nombre='" + lbl_usuario.getText().toString() + "'");
                    }

                }
                finish();
            }
        });

        btn_borrar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (cosa.getString("Permiso").equals("admin")){
                    Cursor c = dbUsr.rawQuery(" SELECT nombre,password,permisos FROM TblUsuarios WHERE permisos='admin'", null);
                    if (c.getCount()>1) {
                        dbUsr.execSQL("DELETE FROM TblMovimientosStock WHERE nombre_usuario="+lbl_usuario.getText().toString() +"'");
                        dbUsr.execSQL("DELETE FROM TblUsuarios WHERE nombre='"+lbl_usuario.getText().toString() + "'");
                        finish();
                    }else{
                        Context context = getApplicationContext();
                        CharSequence text = "Es el ultimo admin, no se puede borrar";
                        int duration = Toast.LENGTH_SHORT;

                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                        return;
                    }
                }else{
                    dbUsr.execSQL("DELETE FROM TblUsuarios WHERE nombre='"+lbl_usuario.getText().toString() + "'");
                    finish();
                }

            }
        });

    }
}
