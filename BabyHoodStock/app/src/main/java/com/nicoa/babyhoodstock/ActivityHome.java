package com.nicoa.babyhoodstock;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

public class ActivityHome extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        final Bundle cosa = this.getIntent().getExtras();

        Toolbar adminToolbar = (Toolbar) findViewById(R.id.adminToolbar);

        setSupportActionBar(adminToolbar);

        // Get the ViewPager and set it's PagerAdapter so that it can display items
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        HomeFragmentPagerAdapter FA = new HomeFragmentPagerAdapter(getSupportFragmentManager(), ActivityHome.this);

        if ((cosa.getString("Permiso").equals("admin"))){
            FA.setPage_count(4);
        }

        viewPager.setAdapter(FA);

        // Give the TabLayout the ViewPager
        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setupWithViewPager(viewPager);
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }



}