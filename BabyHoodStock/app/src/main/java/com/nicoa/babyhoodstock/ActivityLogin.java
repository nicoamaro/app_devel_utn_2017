package com.nicoa.babyhoodstock;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ActivityLogin extends AppCompatActivity {

    public Button btn_login;
    public EditText txt_nombre;
    public EditText txt_password;
    public SQLiteDatabase dbStock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btn_login = (Button) findViewById(R.id.btn_login);
        txt_nombre = (EditText) findViewById(R.id.txt_nombre);
        txt_password = (EditText) findViewById(R.id.txt_password);

        BabyHoodStockSQLiteHelper dbStockHelper = new BabyHoodStockSQLiteHelper(this, "dbBabyHoodStock", null, 1);
        dbStock = dbStockHelper.getReadableDatabase();

        txt_nombre.setText("admin");//FIXME: borrar esto despues de debug
        txt_password.setText("admin");//FIXME: borrar esto despues de debug

        btn_login.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                Cursor c = dbStock.rawQuery(" SELECT nombre,password,permisos FROM TblUsuarios WHERE nombre='"
                        + txt_nombre.getText().toString() +"' AND password='"+ Crypto.md5(txt_password.getText().toString()) +"'", null);

                if (c.getCount()==1){

                    Intent intent = new Intent(ActivityLogin.this, ActivityHome.class);
                    Bundle cosa = new Bundle();
                    c.moveToFirst();
                    cosa.putString("Usuario", txt_nombre.getText().toString());
                    cosa.putString("Permiso", c.getString(2));
                    intent.putExtras(cosa);
                    startActivity(intent);
                }else{
                    Context context = getApplicationContext();
                    CharSequence text = "Usuario y/o contraseña incorrectos";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();

                }

            }
        });
    }

}
