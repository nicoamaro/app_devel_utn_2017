package com.nicoa.babyhoodstock;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Maggie on 10/8/17.
 */
public class FragmentMovimientosList extends Fragment {


    public Movimiento[] movimientos;

    public ListView lst_view_movimientos;
    public FloatingActionButton btn_agregar_movimiento;


    public static FragmentMovimientosList newInstance() {
        FragmentMovimientosList fragment = new FragmentMovimientosList();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movimientos_list, container, false);

        return view;
    }

    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);
        lst_view_movimientos = (ListView)getView().findViewById(R.id.lst_view_movimientos);
        btn_agregar_movimiento = (FloatingActionButton) getView().findViewById(R.id.btn_agregar_movimiento);
        registerForContextMenu(lst_view_movimientos);
        btn_agregar_movimiento.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getView().getContext(), ActivityAgregarMovimiento.class);

                startActivity(intent);

            }
        });

        lst_view_movimientos.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    }

    @Override
    public void onStart(){
        super.onStart();

        cargarMovimientos();

    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;

        menu.setHeaderTitle("Configuracion");
        inflater.inflate(R.menu.menu_movimientos, menu);
    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        int position = ((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position;

        BabyHoodStockSQLiteHelper dbMovimientosHelper = new BabyHoodStockSQLiteHelper(getView().getContext(), "dbBabyHoodStock", null, 1);
        SQLiteDatabase dbMovimientos = dbMovimientosHelper.getWritableDatabase();
        switch (item.getItemId()) {

            case R.id.CtxLblDelete:
                dbMovimientos.execSQL("DELETE FROM TblMovimientosStock WHERE id_movimiento =" + String.valueOf(movimientos[position].getId_movimiento()));
                Toast toast1 = Toast.makeText(getActivity(), "Movimiento Eliminado", Toast.LENGTH_SHORT);
                toast1.setDuration(Toast.LENGTH_SHORT);
                toast1.show();
                cargarMovimientos();
                return true;
            case R.id.CtxLblEdit:
                Intent intent = new Intent(getView().getContext(), ActivityAgregarMovimiento.class);
                Bundle boleta = new Bundle();
                boleta.putString( "Action","Edit");
                boleta.putString( "id_movimientos",String.valueOf(movimientos[position].getId_movimiento() ));
                intent.putExtras(boleta);
                startActivity(intent);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    void cargarMovimientos(){

        BabyHoodStockSQLiteHelper dbMovimientosHelper = new BabyHoodStockSQLiteHelper(getView().getContext(), "dbBabyHoodStock", null, 1);

        SQLiteDatabase dbMovimientos = dbMovimientosHelper.getReadableDatabase();
        Cursor c = dbMovimientos.rawQuery(" SELECT id_movimiento,nombre_usuario,id_prenda,tipo_movimiento,cantidad FROM TblMovimientosStock ", null);

        movimientos = new Movimiento[c.getCount()];
        int i=0;
        //Nos aseguramos de que existe al menos un registro
        if (c.moveToFirst()) {
            //Recorremos el cursor hasta que no haya más registros
            do {

                int id_movimiento       = c.getInt(0);
                String nombre_usuario   = c.getString(1);
                int id_prenda           = c.getInt(2);
                String tipo_movimiento  = c.getString(3);
                int cantidad            = c.getInt(4);

                Cursor cursor_prenda = dbMovimientos.rawQuery(" SELECT id_prenda,nombre_prenda,descripcion,path_foto,talle FROM TblPrendas WHERE id_prenda="+id_prenda, null);
                cursor_prenda.moveToNext();

                String nombre_prenda    = cursor_prenda.getString(1);
                String descripcion      = cursor_prenda.getString(2);
                String path_foto        = cursor_prenda.getString(3);
                String talle            = cursor_prenda.getString(4);

                Prenda prenda = new Prenda(id_prenda,nombre_prenda,descripcion,path_foto,talle,0);

                movimientos[i] = new Movimiento(id_movimiento,nombre_usuario,prenda,tipo_movimiento,cantidad);
                i++;

            } while(c.moveToNext());
        }

        AdaptadorPrendaMovimientos adaptador  = new AdaptadorPrendaMovimientos(getView().getContext(), movimientos);
        lst_view_movimientos.setAdapter(adaptador);
    }



    class AdaptadorPrendaMovimientos extends ArrayAdapter<Movimiento> {

        public AdaptadorPrendaMovimientos(Context context, Movimiento[] movimientos) {
            super(context, R.layout.item_list_movimiento, movimientos);
        }

        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = LayoutInflater.from(getContext());

            View item = inflater.inflate(R.layout.item_list_movimiento, null);

            TextView lbl_tipo_movimiento = (TextView)item.findViewById(R.id.lbl_tipo_movimiento);
            TextView lbl_cantidad = (TextView) item.findViewById(R.id.lbl_cantidad);
            TextView lbl_nombre_prenda = (TextView) item.findViewById(R.id.lbl_nombre_prenda);
            TextView lbl_talle = (TextView) item.findViewById(R.id.lbl_talle);
            TextView nombre_usuario = (TextView) item.findViewById(R.id.nombre_usuario);

            lbl_tipo_movimiento.setText(movimientos[position].getTipo_movimiento());
            lbl_cantidad.setText(String.valueOf(movimientos[position].getCantidad()));
            lbl_nombre_prenda.setText(movimientos[position].getPrenda().getNombre_prenda());
            lbl_talle.setText(movimientos[position].getPrenda().getTalle());
            nombre_usuario.setText(movimientos[position].getNombre_usuario());
            return (item);
        }
    }
}

