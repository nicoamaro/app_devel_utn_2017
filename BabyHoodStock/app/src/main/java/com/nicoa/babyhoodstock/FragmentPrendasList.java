package com.nicoa.babyhoodstock;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Maggie on 10/8/17.
 */
public class FragmentPrendasList extends Fragment {

    public Prenda[] prendas;
    public ListView lst_view_prendas;
    public FloatingActionButton btn_agregar_prenda;


    public static FragmentPrendasList newInstance() {
        FragmentPrendasList fragment = new FragmentPrendasList();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_prendas_list, container, false);
        return view;
    }

    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);
        lst_view_prendas = (ListView)getView().findViewById(R.id.lst_view_prendas);
        btn_agregar_prenda = (FloatingActionButton) getView().findViewById(R.id.btn_agregar_prenda);

        btn_agregar_prenda.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getView().getContext(), ActivityAgregarPrenda.class);

                startActivity(intent);

            }
        });

        lst_view_prendas.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        lst_view_prendas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                Intent intent = new Intent(getView().getContext(), ActivityEditarPrenda.class);
                Bundle cosa = new Bundle();

                cosa.putInt("id_prenda",((Prenda)a.getItemAtPosition(position)).getIdPrenda());


                intent.putExtras(cosa);
                startActivity(intent);

            }
        });
    }

    @Override
    public void onStart(){
        super.onStart();
        BabyHoodStockSQLiteHelper dbUsrHelper = new BabyHoodStockSQLiteHelper(getView().getContext(), "dbBabyHoodStock", null, 1);
        SQLiteDatabase dbPrendas = dbUsrHelper.getReadableDatabase();
        Cursor c = dbPrendas.rawQuery(" SELECT id_prenda,nombre_prenda,descripcion,path_foto,talle FROM TblPrendas ", null);

        prendas = new Prenda[c.getCount()];
        int i=0;
        //Nos aseguramos de que existe al menos un registro
        if (c.moveToFirst()) {
            //Recorremos el cursor hasta que no haya más registros
            do {
                int id_prenda           = c.getInt(0);
                String nombre_prenda    = c.getString(1);
                String descripcion      = c.getString(2);
                String path_foto        = c.getString(3);
                String talle            = c.getString(4);

                prendas[i] = new Prenda(id_prenda,nombre_prenda,descripcion,path_foto,talle,0);
                i++;

            } while(c.moveToNext());
        }

        AdaptadorPrenda adaptador  = new AdaptadorPrenda(getView().getContext(), prendas);
        lst_view_prendas.setAdapter(adaptador);

    }





    class AdaptadorPrenda extends ArrayAdapter<Prenda> {

        public AdaptadorPrenda(Context context, Prenda[] prendas) {
            super(context, R.layout.item_list_prenda, prendas);
        }

        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = LayoutInflater.from(getContext());

            View item = inflater.inflate(R.layout.item_list_prenda, null);

            TextView lbl_nombre_prenda = (TextView) item.findViewById(R.id.lbl_nombre_prenda);
            TextView lbl_talle = (TextView) item.findViewById(R.id.lbl_talle);

            lbl_nombre_prenda.setText(prendas[position].getNombre_prenda());
            lbl_talle.setText(prendas[position].getTalle());
            return (item);
        }
    }
}

