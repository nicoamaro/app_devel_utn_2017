package com.nicoa.babyhoodstock;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by Maggie on 10/8/17.
 */
public class FragmentStockList extends Fragment {

    public Prenda[] prendas;
    public ListView lst_view_stock;
    public FloatingActionButton btn_agregar_movimiento;


    public static FragmentStockList newInstance() {
        FragmentStockList fragment = new FragmentStockList();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stock_list, container, false);
        return view;
    }

    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);
        lst_view_stock = (ListView)getView().findViewById(R.id.lst_view_stock);
        btn_agregar_movimiento = (FloatingActionButton) getView().findViewById(R.id.btn_agregar_movimiento);

        btn_agregar_movimiento.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               // Intent intent = new Intent(getView().getContext(), ActivityAgregarMovimiento.class);

               // startActivity(intent);

            }
        });

        lst_view_stock.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        lst_view_stock.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                //Intent intent = new Intent(getView().getContext(), ActivityEditarPrenda.class);
                //Bundle cosa = new Bundle();

                //cosa.putInt("id_prenda",((Prenda)a.getItemAtPosition(position)).getIdPrenda());


                //intent.putExtras(cosa);
                //startActivity(intent);

            }
        });
    }

    @Override
    public void onStart(){
        super.onStart();
        BabyHoodStockSQLiteHelper dbUsrHelper = new BabyHoodStockSQLiteHelper(getView().getContext(), "dbBabyHoodStock", null, 1);
        SQLiteDatabase dbPrendas = dbUsrHelper.getReadableDatabase();
        Cursor c = dbPrendas.rawQuery(" SELECT id_prenda,nombre_prenda,descripcion,path_foto,talle FROM TblPrendas ", null);

        prendas = new Prenda[c.getCount()];
        int i=0;
        //Nos aseguramos de que existe al menos un registro
        if (c.moveToFirst()) {
            //Recorremos el cursor hasta que no haya más registros
            do {
                int id_prenda           = c.getInt(0);
                String nombre_prenda    = c.getString(1);
                String descripcion      = c.getString(2);
                String path_foto        = c.getString(3);
                String talle            = c.getString(4);

                prendas[i] = new Prenda(id_prenda,nombre_prenda,descripcion,path_foto,talle,0);
                i++;

            } while(c.moveToNext());
        }
        int prendas_qty = c.getCount();
        int acumulador_movimientos = 0;
        int cantidad = 0;
        String tipo = "";
        int signo = 1;

        for(i=0;i<prendas_qty;i++){

            c = dbPrendas.rawQuery(" SELECT tipo_movimiento,cantidad FROM TblMovimientosStock WHERE id_prenda="+prendas[i].getIdPrenda(), null);
            if (c.moveToFirst()) {
                for (int j=0;j<c.getCount();j++){
                    tipo = c.getString(0);
                    cantidad = c.getInt(1);
                    prendas[i].ingresarMovimiento(tipo,cantidad);
                    c.moveToNext();
                }
            }
        }

        AdaptadorPrendaStock adaptador  = new AdaptadorPrendaStock(getView().getContext(), prendas);
        lst_view_stock.setAdapter(adaptador);

    }





    class AdaptadorPrendaStock extends ArrayAdapter<Prenda> {

        public AdaptadorPrendaStock(Context context, Prenda[] prendas) {
            super(context, R.layout.item_list_stock, prendas);
        }

        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = LayoutInflater.from(getContext());

            View item = inflater.inflate(R.layout.item_list_stock, null);

            TextView lbl_nombre_prenda = (TextView) item.findViewById(R.id.lbl_nombre_prenda);
            TextView lbl_talle = (TextView) item.findViewById(R.id.lbl_talle);
            TextView lbl_stock = (TextView) item.findViewById(R.id.lbl_stock);


            lbl_nombre_prenda.setText(prendas[position].getNombre_prenda());
            lbl_talle.setText(prendas[position].getTalle());
            lbl_stock.setText(String.valueOf(prendas[position].getStock_disponible()));


            return (item);
        }
    }
}

