package com.nicoa.babyhoodstock;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by Maggie on 10/7/17.
 */


public class HomeFragmentPagerAdapter extends FragmentPagerAdapter {
    private int page_count = 3;
    private  String tabTitles[] = new String[] { "Stock", "Movimientos","Prendas", "Usuarios" };
    private Context context;

    public HomeFragmentPagerAdapter(FragmentManager fm, Context context) {

        super(fm);
        this.context = context;
    }

    public void setPage_count(int PAGE_COUNT) {
        this.page_count = PAGE_COUNT;
    }

    @Override
    public int getCount() {
        return page_count;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment f = null;

        switch(position) {
            case 0:
                f = FragmentStockList.newInstance();
                break;
            case 1:
                f = FragmentMovimientosList.newInstance();
                break;
            case 2:
                f = FragmentPrendasList.newInstance();
                break;
            case 3:
                f = FragmentUserList.newInstance();
                break;
        }

        return f;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }
}
