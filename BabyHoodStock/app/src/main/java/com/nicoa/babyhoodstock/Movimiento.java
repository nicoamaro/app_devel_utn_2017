package com.nicoa.babyhoodstock;

/**
 * Created by Maggie on 10/11/17.
 */

public class Movimiento {
    //id_movimiento,nombre_usuario,id_prenda,tipo_movimiento,cantidad
    private int id_movimiento;
    private String nombre_usuario;
    private Prenda prenda;
    private String tipo_movimiento;
    private int cantidad;

    public Movimiento(int id_movimiento, String nombre_usuario, Prenda prenda, String tipo_movimiento, int cantidad){
        this.id_movimiento = id_movimiento;
        this.nombre_usuario = nombre_usuario;
        this.prenda = new Prenda(prenda.getIdPrenda(),prenda.getNombre_prenda(),prenda.getDescripcion(),prenda.getPath_foto(),prenda.getTalle(),prenda.getStock_disponible());
        this.tipo_movimiento = tipo_movimiento;
        this.cantidad = cantidad;
    }



    public int getId_movimiento() {
        return id_movimiento;
    }

    public Prenda getPrenda() {
        return prenda;
    }

    public String getNombre_usuario() {
        return nombre_usuario;
    }

    public String getTipo_movimiento() {
        return tipo_movimiento;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public void setId_movimiento(int id_movimiento) {
        this.id_movimiento = id_movimiento;
    }

    public void setNombre_usuario(String nombre_usuario) {
        this.nombre_usuario = nombre_usuario;
    }

    public void setTipo_movimiento(String tipo_movimiento) {
        this.tipo_movimiento = tipo_movimiento;
    }

    public void setPrenda(Prenda prenda) {
        this.prenda.setIdPrenda(prenda.getIdPrenda());
        this.prenda.setNombre_prenda(prenda.getNombre_prenda());
        this.prenda.setDescripcion(prenda.getDescripcion());
        this.prenda.setNombre_prenda(prenda.getPath_foto());
        this.prenda.setTalle(prenda.getTalle());
        this.prenda.setStock_disponible(prenda.getStock_disponible());


    }
}

