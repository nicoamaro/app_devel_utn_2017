package com.nicoa.babyhoodstock;

/**
 * Created by Maggie on 10/5/17.
 */

public class Prenda {

    private int id_prenda;
    private String nombre_prenda;
    private String descripcion;
    private String path_foto;
    private String talle;
    private int stock_disponible;


    public Prenda(int _id_prenda, String _nombre_prenda, String _descripcion, String _path_foto, String _talle, int _stock_disponible){
        id_prenda=_id_prenda;
        nombre_prenda=_nombre_prenda;
        descripcion=_descripcion;
        path_foto=_path_foto;
        talle=_talle;
        stock_disponible = _stock_disponible;
    }

    public int getIdPrenda() {
        return id_prenda;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public String getPath_foto() {
        return path_foto;
    }
    public String getNombre_prenda() {
        return nombre_prenda;
    }
    public int getStock_disponible() {
        return stock_disponible;
    }
    public String getTalle() {
        return talle;
    }

    public void setDescripcion(String descripcion) {

        this.descripcion = descripcion;
    }

    public void setPath_foto(String foto_path) {

        this.path_foto = foto_path;
    }

    public void setIdPrenda(int _id_prenda) {

        this.id_prenda = _id_prenda;
    }

    public void setNombre_prenda(String nombre_prenda) {

        this.nombre_prenda = nombre_prenda;
    }

    public void setStock_disponible(int stock_disponible) {
        this.stock_disponible = stock_disponible;
    }

    public void setTalle(String talle) {
        this.talle = talle;
    }

    public int ingresarMovimiento(String _tipo, int _canditad){
        int signo = 0;

        if(_tipo.equals("Entrada")){
            signo=1;
        }else if (_tipo.equals("Salida")){
            signo=-1;
        }else{
            return -1;
        }

        stock_disponible+=(signo*_canditad);
        return 0;
    }

}

