package com.nicoa.babyhoodstock;

/**
 * Created by Maggie on 10/5/17.
 */

public class Usuario {

    private String usuario;
    private String password;
    private String permiso;


    public Usuario(String _usuario, String _password, String _permiso){
        usuario = _usuario;
        password = _password;
        permiso = _permiso;
    }

    public String getPassword() {
        return password;
    }

    public String getPermiso() {
        return permiso;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String _usuario) {
        usuario = _usuario;
    }
    public void setPassword(String _password) {
        password = Crypto.md5(_password);
    }
    public void setPermiso(String _permiso) {
        permiso = _permiso;
    }


}
