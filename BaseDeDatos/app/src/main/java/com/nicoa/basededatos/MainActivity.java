package com.nicoa.basededatos;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.database.sqlite.SQLiteDatabase;
import android.widget.ListView;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    public Button btnAgregar;
    public Button btnBorrar;
    public Button btnModificar;
    public ListView lstView;
    public Usuario[] usuarios;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnAgregar = (Button)findViewById(R.id.btnAgregar);
        btnBorrar = (Button)findViewById(R.id.btnBorrar);
        btnModificar = (Button)findViewById(R.id.btnModificar);
        lstView = (ListView)findViewById(R.id.lstView);

        UsuariosSQLiteHelper dbUsrHelper = new UsuariosSQLiteHelper(this, "DBUsuarios", null, 1);
        SQLiteDatabase dbUsr = dbUsrHelper.getWritableDatabase();
        Cursor c = dbUsr.rawQuery(" SELECT nombre,apellido,edad FROM Usuarios ", null);

        usuarios = new Usuario[c.getCount()];
        int i=0;
        //Nos aseguramos de que existe al menos un registro
        if (c.moveToFirst()) {
            //Recorremos el cursor hasta que no haya más registros
            do {
                String nombre = c.getString(0);
                String apellido = c.getString(1);
                int edad = c.getInt(2);

                usuarios[i] = new Usuario(nombre,apellido,edad);
                i++;

            } while(c.moveToNext());
        }
        AdaptadorUsuario adaptador  = new AdaptadorUsuario(this, usuarios);

        lstView.setAdapter(adaptador);

        lstView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                //Alternativa 1:
                String opcionSeleccionada = ((Usuario)a.getItemAtPosition(position)).getNombre();


            }
        });


        btnAgregar.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AgregarActivity.class);
                startActivity(intent);
            }
        });

        btnBorrar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

            }
        });

        btnModificar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


            }
        });


    }

    class AdaptadorUsuario extends ArrayAdapter<Usuario> {

        public AdaptadorUsuario(Context context, Usuario[] usuarios) {
            super(context, R.layout.db_list_item_layout, usuarios);
        }

        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = LayoutInflater.from(getContext());

            View item = inflater.inflate(R.layout.db_list_item_layout, null);

            TextView lbl_nombre = (TextView) item.findViewById(R.id.lbl_nombre);
            TextView lbl_apellido = (TextView) item.findViewById(R.id.lbl_apellido);
            TextView lbl_edad = (TextView) item.findViewById(R.id.lbl_edad);

            lbl_nombre.setText(usuarios[position].getNombre());
            lbl_apellido.setText(usuarios[position].getApellido());
            lbl_edad.setText( String.valueOf(usuarios[position].getEdad()) );


            return (item);
        }
    }


}
