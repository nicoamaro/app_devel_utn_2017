package com.nicoa.basededatos;

/**
 * Created by Maggie on 9/12/17.
 */

public class Usuario {

    private String nombre;
    private String apellido;
    private int edad;

    public Usuario(String _nombre, String _apellido, int _edad){
        nombre = _nombre;
        apellido = _apellido;
        edad = _edad;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public int getEdad() {
        return edad;
    }


    public void setNombre(String _nombre) {
        nombre = _nombre;
    }
    public void setApellido(String _apellido) {
        apellido = _apellido;
    }
    public void setEdad(int _edad) {
        edad = _edad;
    }


}
