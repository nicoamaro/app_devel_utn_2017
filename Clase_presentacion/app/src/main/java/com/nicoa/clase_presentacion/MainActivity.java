package com.nicoa.clase_presentacion;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
//import android.view.*;

import java.util.regex.PatternSyntaxException;

public class MainActivity extends AppCompatActivity {

    public TextView txt_main;
    public Button btn_escribir;
    public Button btn_borrar;
    public Button btn_increase;
    public Button btn_decrease;
    public float text_size = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txt_main=(TextView)findViewById(R.id.txt_main);
        btn_escribir = (Button)findViewById(R.id.btn_escribir);
        btn_borrar = (Button)findViewById(R.id.btn_borrar);
        btn_increase = (Button)findViewById(R.id.btn_increase);
        btn_decrease = (Button)findViewById(R.id.btn_decrease);

        btn_escribir.setText("Escribir");
        btn_borrar.setText("Borrar");
        btn_increase.setText("Increase");
        btn_decrease.setText("Decrease");



        txt_main.setTextSize(text_size);

        btn_escribir.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                txt_main.setText("Hello");
            }
        });

        btn_borrar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                txt_main.setText("");
            }
        });

        btn_increase.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                //txt_main.setTextSize(txt_main.getTextSize()+(float) 1.0);
                text_size = txt_main.getTextSize()*(float)1.1;
                txt_main.setTextSize(text_size);
            }
        });
        btn_decrease.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                // Code here executes on main thread after user presses button
                //txt_main.setTextSize(txt_main.getTextSize()-(float) 1.0);
                text_size = txt_main.getTextSize()*(float)0.2;
                txt_main.setTextSize(text_size);
                //txt_main.setTextSize(text_size--);
            }
        });


    }


}
