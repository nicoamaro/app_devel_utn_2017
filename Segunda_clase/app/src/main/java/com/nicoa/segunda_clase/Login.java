package com.nicoa.segunda_clase;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;




public class Login extends AppCompatActivity {

    public EditText txt_nombre;
    public EditText txt_password;
    public Button   btn_login;
    public TextView lbl_intentos;
    public int      tries=0;
    public static int  MAX_TRIES=3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txt_nombre = (EditText)findViewById(R.id.txt_nombre);
        txt_password = (EditText)findViewById(R.id.txt_password);
        btn_login  = (Button)findViewById(R.id.btn_login);
        lbl_intentos = (TextView)findViewById(R.id.lbl_intentos);

        lbl_intentos.setText("Intentos Usados: "+ tries + "\nIntentos Disponibles:" + (MAX_TRIES - tries) );

        btn_login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Login.this, MainActivity.class);

                Bundle cosa = new Bundle();
                tries++;

                if(  txt_nombre.getText().toString().equals(txt_password.getText().toString())) {

                    cosa.putString("Nombre", txt_nombre.getText().toString());
                    cosa.putString("Password", txt_password.getText().toString());

                    intent.putExtras(cosa);
                    startActivity(intent);
                    tries=0;
                }else if (tries < MAX_TRIES){

                    Context context = getApplicationContext();
                    CharSequence text = "Pusiste mal la contraseña!";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();


                }else{
                    Context context = getApplicationContext();
                    CharSequence text = "Pusiste mal la contraseña muchas veces, cerra y volve a abrir!";;
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                    btn_login.setVisibility(View.INVISIBLE);
                }
                lbl_intentos.setText("Intentos Usados: "+ tries + "\nIntentos Disponibles:" + (MAX_TRIES - tries) );
            }
        });
    }
}
