package com.nicoa.segunda_clase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public TextView lbl_saludo;
    public TextView lbl_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lbl_saludo = (TextView)findViewById(R.id.lbl_saludo);
        lbl_password = (TextView)findViewById(R.id.lbl_password);


        Bundle cosa = this.getIntent().getExtras();

        lbl_saludo.setText("Hola "+ cosa.getString("Nombre"));
        lbl_password.setText("Tu contraseña es: " + cosa.getString("Password"));


    }
}
