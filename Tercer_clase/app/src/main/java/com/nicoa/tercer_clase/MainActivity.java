package com.nicoa.tercer_clase;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    public Button btn_spinner;
    public Button btn_list_view;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_spinner  = (Button)findViewById(R.id.btn_spinner);
        btn_list_view  = (Button)findViewById(R.id.btn_list_view);

        btn_spinner.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, spinner_activity.class);
                startActivity(intent);
            }
        });
        btn_list_view.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, list_view_activity.class);
                startActivity(intent);
            }
        });
    }
}
