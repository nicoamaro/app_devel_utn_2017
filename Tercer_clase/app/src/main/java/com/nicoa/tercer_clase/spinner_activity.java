package com.nicoa.tercer_clase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

public class spinner_activity extends AppCompatActivity {

    public Spinner spn_datos;
    public TextView txt_ver_datos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner_activity);

        spn_datos = (Spinner)findViewById(R.id.spn_datos);
        txt_ver_datos = (TextView)findViewById(R.id.txt_ver_datos);

        final String[] datos =
                new String[]{"Elem1","Elem2","Elem3","Elem4","Elem5"};

        ArrayAdapter<String> adaptador =
                new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_item, datos);



        adaptador.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);

        spn_datos.setAdapter(adaptador);

        spn_datos.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent,
                                               android.view.View v, int position, long id) {
                        txt_ver_datos.setText("Seleccionado: " +
                                parent.getItemAtPosition(position));
                    }

                    public void onNothingSelected(AdapterView<?> parent) {
                        txt_ver_datos.setText("");
                    }
                });



    }
}
